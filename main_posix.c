#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>


int current_index, current_move_number;
char** parse_pgn(char* game){
    int max_subresult_length = 7;
    current_index = 0, current_move_number = 0;
    char** result = (char**)malloc(0*sizeof(char));


    return result;
}

int main(){

    char filename[1024], game[2048];
    printf("Enter filename: ");
    scanf("%s", filename);
    int file = open(filename, O_RDONLY);
    if(file == -1){
        printf("No such file.\n");
        return -1;
    }
    int read_bytes = read(file, game, 2048);
    game[read_bytes] = '\0';

    char** result = parse_pgn(game);

    printf("%s ...\n", game);

    for(int i = 0; i < current_move_number; ++i){
        if(i % 2 == 0){
            printf("White's %d. move: %s\n", (i/2)+1, result[i]);
        }
        else{
            printf("Black's %d. move: %s\n", (i/2)+1, result[i]);
        }
    }

    for(int i = 0; i < current_move_number; ++i){
        free(result[i]);
    }
    free(result);


}
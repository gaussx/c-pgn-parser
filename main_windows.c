#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>


int current_index, current_move_number;

void purge(char* string, int n){
    for(int i = 0; i < n; ++i){
        string[i] = '\0';
    }
}

char** initialize_board(){
    //The 1st rank - board[0][X], The 2nd rank - board[1][X] etc.
    //The A file - board[X][0], the B file - board[X][1] etc.
    char** board = (char**)malloc(sizeof(char*) * 8);
    for(int i = 0; i < 8; ++i){
        board[i] = (char*)malloc(sizeof(char) * 8);
    }
    strcpy(board[0], "RNBQKBNR");
    strcpy(board[1], "PPPPPPPP");
    strcpy(board[2], "        ");
    strcpy(board[3], "        ");
    strcpy(board[4], "        ");
    strcpy(board[5], "        ");
    strcpy(board[6], "pppppppp");
    strcpy(board[7], "rnbqkbnr");

    return board;
}



void print_board(char** board, char perspective){
    switch(perspective){
        case 'b':
            printf("    ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╤═══╗\n");
            printf("    ║ H │ G │ F │ E │ D │ C │ B │ A ║\n"); // │ ┼ ─ ╔ ╗ ╚ ╧ ╝ ╟ ╪ ╬ ═ ║ ╤
            printf("╔═══╬═══╪═══╪═══╪═══╪═══╪═══╪═══╪═══╬═══╗\n");
            for(int i = 0; i < 8; ++i){
                printf("║ %d ║", i+1);
                for(int j = 7; j > -1; --j){
                    printf(" %c ", board[i][j]);
                    if(j != 0){
                        printf("│");
                    }
                }
                printf("║ %d ║\n", i+1);
                if(i != 7){
                    printf("╟───╫───┼───┼───┼───┼───┼───┼───┼───╫───╢\n");
                }
                else{
                    printf("╚═══╬═══╪═══╪═══╪═══╪═══╪═══╪═══╪═══╬═══╝\n");
                    printf("    ║ H │ G │ F │ E │ D │ C │ B │ A ║\n");
                    printf("    ╚═══╧═══╧═══╧═══╧═══╧═══╧═══╧═══╝\n");
                }
            }
            break;
        case 'w':
            printf("    ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╤═══╗\n");
            printf("    ║ A │ B │ C │ D │ E │ F │ G │ H ║\n"); // │ ┼ ─ ╔ ╗ ╚ ╧ ╝ ╟ ╪ ╬ ═ ║ ╤
            printf("╔═══╬═══╪═══╪═══╪═══╪═══╪═══╪═══╪═══╬═══╗\n");
            for(int i = 7; i > -1; --i){
                printf("║ %d ║", i+1);
                for(int j = 0; j < 8; ++j){
                    printf(" %c ", board[i][j]);
                    if(j != 7){
                        printf("│");
                    }
                }
                printf("║ %d ║\n", i+1);
                if(i != 0){
                    printf("╟───╫───┼───┼───┼───┼───┼───┼───┼───╫───╢\n");
                }
                else{
                    printf("╚═══╬═══╪═══╪═══╪═══╪═══╪═══╪═══╪═══╬═══╝\n");
                    printf("    ║ A │ B │ C │ D │ E │ F │ G │ H ║\n");
                    printf("    ╚═══╧═══╧═══╧═══╧═══╧═══╧═══╧═══╝\n");
                }
            }
            break;
        default:
            printf("Niepoprawna perspektywa.\n");
            break;
    }
    return;
}

//zamiana z tego co podaje uzytkownik na pozycje x i y

char *position_to_code(int info[2]){
    char *result = (char*)malloc(2*sizeof(char));
    result[0] = info[1] + 'a';
    result[1] = info[0] + '1';
    return result;
}

int *code_to_position(char kod[2]){
    int *pozycja = (int*)malloc(2*sizeof(int));
    pozycja[0] = kod[1] - '1';
    pozycja[1] = kod[0] - 'a';
    return pozycja;
}

char* move_to_description(char** board, char* move, int whose_move){
    char* result = (char*)malloc(sizeof(char) * 4);
    char piece = move[0];
    //deranged but effective idea to keep the code shorter:
    //add 32*(whose move) to determine what colour piece we're looking for
    //since 0 is white's move, and 1 is black's move,
    //and black pieces are represented by small letters, each of which is represented by a value
    //equal to CORRESPONDING_BIG_LETTER_VALUE + 32
    
    char correct_piece = piece + whose_move*32;
    if(move[0] >= 'a' && move[0] <= 'h') piece = 'P';
    if(move[1] >= 'a' && move[1] <= 'h' && move[2] >= 'a' && move[2] <= 'h'){
        //then there's no problem, we already have all information we need noted down.
        for(int i = 0; i < 8; ++i){
            if(board[move[1] - 'a'][i] == correct_piece){
                result[0] = move[1];
                result[1] = i + '1';
                break;
            }
        }
        result[2] = move[2];
        result[3] = move[3];
        return result;
    }
    if(move[1] >= '1' && move[1] <= '8' && move[2] >= 'a' && move[2] <= 'h'){
        //another obvious case.
        //let's say we have R8h7, and we want h8h7 as the result.
        result[0] = move[2];
        result[1] = move[1];
        result[2] = move[2];
        result[3] = move[3];

        return result;
    }
    int* end_position = code_to_position(move);
    
    if(piece != 'P'){
        for(int i = 0; i < 8; ++i){
            for(int j = 0; j < 8; ++j){
                if(board[i][j] == correct_piece){
                    switch(piece){
                        case 'K':{

                            break;
                        }
                    }
                }
            }
        }
    }

    free(end_position);

    return result;
}


char** parse_pgn(char* game){
    int max_subresult_length = 7, current_substring_index = 0, max_pgn_length = 10;
    current_index = 0, current_move_number = 0;

    char current_move[max_pgn_length];
    char** result = (char**)malloc(0*sizeof(char));

    char** board = initialize_board();

    print_board(board, 'w');
    while(game[current_index] != '\0'){
        if(game[current_index] == '['){
            while(game[++current_index] != ']') continue;
            continue;
        }
        else if(game[current_index] == '{'){
            while(game[++current_index] != '}') continue;
            continue;
        }
        else if(game[current_index] != ' '){
            if((game[current_index] >= 'A' && game[current_index] <= 'Z') || (game[current_index] >= 'a' && game[current_index] <= 'z')){
                current_substring_index = 0;
                purge(current_move, max_pgn_length);
                while(game[current_index] != ' ' && game[current_index] != '\n') current_move[current_substring_index++] = game[current_index++];
                ++current_move_number;
                result = realloc(result, current_move_number*sizeof(char*));
                result[current_move_number - 1] = (char*)malloc(max_subresult_length*sizeof(char));
                purge(result[current_move_number - 1], max_pgn_length);
                strcpy(result[current_move_number - 1], current_move);
            }
        }
        ++current_index;
    }

    for(int i = 0; i < 8; ++i){
        free(board[i]);
    }
    free(board);

    return result;
}
//osn - our standarized notation
//syntax: [source][destination][info about game state]
//available info aliases:
//  G - game goes on normally
//  D - declared draw
//  # - mate
//  + - check
//  W - white wins, black resigned
//  B - black wins, white resigned
//example: e2e4G - move from e2 to e4
//example 2: h3h7# - move from h3 to h7, resulted in the opposite king getting mated.
//example 3: a3c5D - move from a3 to c5, after which a draw was declared
//example 4: c4c5+ - move from c4 to c5, which put the enemy king in check.
//castling is denoted as krrk (short) or rkkr (long)
char** parse_osn(char* game){
    int max_subresult_length = 6, current_substring_index = 0;
    current_index = 0, current_move_number = 0;

    char current_move[max_subresult_length];
    char** result = (char**)malloc(0*sizeof(char));
    while(game[current_index] != '\0'){
        if(game[current_index] != ' ' && game[current_index] != '\n'){
            if(game[current_index] >= 'a' && game[current_index] <= 'z'){
                current_substring_index = 0;
                purge(current_move, max_subresult_length);
                while(game[current_index] != ' ' && game[current_index] != '\n' && game[current_index] != '\0') current_move[current_substring_index++] = game[current_index++];
                ++current_move_number;
                result = realloc(result, current_move_number*sizeof(char*));
                result[current_move_number - 1] = (char*)malloc(max_subresult_length*sizeof(char));
                purge(result[current_move_number - 1], max_subresult_length);
                strcpy(result[current_move_number - 1], current_move);
            }
        }
        ++current_index;
    }

    return result;
}

int* osn_to_move(char* osn_move){
    int* result = (int*)malloc(sizeof(int) * 4);
    result[0] = osn_move[1] - '1';
    result[1] = osn_move[0] - 'a';

    result[2] = osn_move[3] - '1';
    result[3] = osn_move[2] - 'a';
    return result;
}

void verbose_state(char* osn_move){
    switch(osn_move[4]){
        case '+':{
            printf("The enemy king is now in check.\n");
            break;
        }
        case '#':{
            printf("The enemy king was mated - victory.\n");
            break;
        }
        case 'D':{
            printf("A draw was declared.\n");
            break;
        }
        case 'W':{
            printf("Black resigned. White is declared the winner.\n");
            break;
        }
        case 'B':{
            printf("White resigned. Black is declared the winner.\n");
            break;
        }
        default:{
            printf("The game goes on.\n");
            break;
        }
    }
}

int main(){

    char filename[1024], game[2048];
    printf("Enter filename: ");
    scanf("%s", filename);
    FILE *file = fopen(filename, "r");
    if(file == NULL){
        printf("No such file.\n");
        return -1;
    }
    int read_bytes = fread(game, sizeof(char), 2048, file);
    game[read_bytes] = '\0';

    // char** result = parse_pgn(game);
    printf("%s ...\n", game);
    printf("%d\n", read_bytes);

    char** result = parse_osn(game);

    for(int i = 0; i < current_move_number; ++i){
        if(i % 2 == 0){
            printf("White's %d. move: %s\n", (i/2)+1, result[i]);
        }
        else{
            printf("Black's %d. move: %s\n", (i/2)+1, result[i]);
        }
        int* test = osn_to_move(result[i]);
        printf("Moves from %d, %d to %d, %d\n", test[0], test[1], test[2], test[3]);
        verbose_state(result[i]);
        free(test);
    }

    fclose(file);
    for(int i = 0; i < current_move_number; ++i){
        free(result[i]);
    }
    free(result);


}